using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchMagic : MonoBehaviour
{

    public GameObject firePoint;
    public GameObject effectToSpawn;
    public bool timeToAttack = false;
    GameObject vfx;
    // Start is called before the first frame update
  
    // Update is called once per frame
    void Update()
    {
        if (timeToAttack)
        {
            SpawnVFX();
            
        }
        
    }

    void SpawnVFX()
    {
        
        if (firePoint != null)
        {
            vfx = Instantiate(effectToSpawn, firePoint.transform.position, Quaternion.identity);
            timeToAttack = false;
        }
   
        Destroy(vfx, 3.5f);
    }   
}

