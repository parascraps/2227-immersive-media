using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossController : MonoBehaviour
{

    public int HP = 50;
    [SerializeField] private Animator animator;

    public GameObject boss;

    public LaunchMagic lm;
    private float rotation;

    [SerializeField] public float xmin = 10;
    [SerializeField] public float xmax = 45;
    [SerializeField] public float zmin = 10;
    [SerializeField] public float zmax = 45;
    [SerializeField] public float warpCooldownIntervalmin = 1.5f;
    [SerializeField] public float warpCooldownIntervalmax = 3.5f;

    [SerializeField] public bool dead = false;
    public AudioClip laugh;
    public AudioClip teleport;
    public AudioClip death;
    public AudioClip attack;
    [SerializeField] public float attackCooldownInterval = 2.0f;

    [SerializeField] public float warptime = 2.0f;
    private bool start2move = true;
    public bool keepMoving = true;

    public float newx = 0;
    public float newz = 0;

    public Transform Opponent;

    // Start is called before the first frame update
    void Start()
    {
        animator.SetBool("dead", false);
   
    }

    // Update is called once per frame.

    public AnimationCurve myCurve;

    IEnumerator Move()
    {
        
        while (keepMoving)
        {
            yield return new WaitForSeconds(Random.Range(warpCooldownIntervalmin, warpCooldownIntervalmax));

            do
            {
                newx = Random.Range(xmin, xmax);
                newz = Random.Range(zmin, zmax);
            }
            while (((28 <= newx) && (newx <= 42)) && ((28 <= newz) && (newz <= 42))); //Avoiding tree
                       
            boss.transform.position = new Vector3(newx, transform.position.y, newz);

            AudioSource teleporting = GetComponent<AudioSource>();
            teleporting.PlayOneShot(teleport);
        }
    }

    void FaceOponent()
    {
        boss.transform.LookAt(Opponent);
    }

    void Hover()
    {
        boss.transform.position = new Vector3(transform.position.x, myCurve.Evaluate((Time.time % myCurve.length)), transform.position.z);
    }


    IEnumerator Attack()
    {
        while (!dead && keepMoving)

        {
            
            lm.timeToAttack = true;
            AudioSource attacking = GetComponent<AudioSource>();
            attacking.PlayOneShot(attack);
            yield return new WaitForSeconds(attackCooldownInterval);
           


        }
    }
    void Update()
    {

        if (HP <= 0 )
        {
            if(keepMoving)
                animator.SetBool("dead", true);
                keepMoving = false;
                boss.transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            if (!dead)
            {
                StartCoroutine("Die");
            }
        }
        else
        {
            Hover();
            FaceOponent();
            if (HP == 45 && start2move == true)
            {
                
                StartCoroutine("Move");
                StartCoroutine("Attack");
                StartCoroutine("Laugh");
                start2move = false;
            }
        }
    }


    IEnumerator Laugh()
    {
        while (HP > 0)
        {
            yield return new WaitForSeconds(6.0f);
            AudioSource laughing = GetComponent<AudioSource>();
            laughing.PlayOneShot(laugh);
            
        }
       
    }

    IEnumerator Die()
    {
        
        AudioSource kill = GetComponent<AudioSource>();
        kill.PlayOneShot(death, 0.2f);
        yield return new WaitForSeconds(1.0f);
        dead = true;
    }

}
