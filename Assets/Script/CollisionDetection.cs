using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour{ 

    public PlayerController pc;
    public GameObject HitParticle;
    public AudioClip swordHitSound;
    public AudioClip damageSound;
    public AudioClip heroHurt;
    public bossController boss;
    private float lastHit = 0;
    public float range = 0.5f;


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy" && pc.isAttacking && (Time.time - lastHit) > 0.5f)
        {
            RaycastHit hit;
            
            if (Physics.Raycast(pc.transform.position, pc.transform.forward, out hit, range))
            {
                AudioSource audioS = GetComponent<AudioSource>();
                audioS.PlayOneShot(swordHitSound);
                AudioSource hitFx = GetComponent<AudioSource>();
                hitFx.PlayOneShot(damageSound);

                other.GetComponent<Animator>().SetTrigger("Hit");
                Instantiate(HitParticle, new Vector3(other.transform.position.x, 
                other.transform.position.y, other.transform.position.z), 
                other.transform.rotation);

                boss.HP = boss.HP - 5;
                lastHit = Time.time;

                print("Boss HP: "+boss.HP);
            }
        }
        else if (other.tag == "EnemyAttack"  && (Time.time - lastHit) > 0.5f)
        {

            pc.GetComponent<Animator>().SetTrigger("Hit");
            Instantiate(HitParticle, new Vector3(pc.transform.position.x,
            pc.transform.position.y, pc.transform.position.z),
            pc.transform.rotation);

            pc.HP = pc.HP - 5;
            lastHit = Time.time;
            AudioSource audioS = GetComponent<AudioSource>();
            audioS.PlayOneShot(heroHurt);

            if(pc.HP == 0)
            {
                pc.GetComponent<Animator>().SetBool("Dead", true);
            }

            print("Hero HP: " + pc.HP);
        }


    }
}
