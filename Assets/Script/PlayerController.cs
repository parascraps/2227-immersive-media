//Based on code found at:
//https://github.com/Acacia-Developer/Unity-FPS-Controller

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Transform playerCamera = null;
    [SerializeField] float mouseSensitivity = 3.5f;
    [SerializeField] float walkSpeed = 5.0f;
    [SerializeField] float jumpHeight = 6.0f;

    public AudioClip jumpSound;
    public int HP = 100;
    private float attackCoolDown = 1.0f;
    public bool canAttack = true;
    private bool canJump = true;
    public AudioClip swordAttackSound;
    //AudioClip swordHitSound;
    public bool isAttacking = false;


    [SerializeField] float runSpeedIncrease = 4.0f;
    [SerializeField] [Range(0.0f, 0.5f)] float moveSmoothTime = 0.3f;
    [SerializeField] [Range(0.0f, 0.5f)] float mouseSmoothTime = 0.03f;

    [SerializeField] private Animator animator;


    [SerializeField] float gravity = -13.0f;


    Vector3 moveDirection;
    Vector3 velocity;



    [SerializeField] bool lockCursor = true;

    float cameraPitch = 0.0f;
    float velocityY = 0.0f;
    float finalSpeed = 0.0f;
    CharacterController controller = null;

    Vector2 currentDir = Vector2.zero;
    Vector2 currentDirVelocity = Vector2.zero;

    Vector2 currentMouseDelta = Vector2.zero;
    Vector2 currentMouseDeltaVelocity = Vector2.zero;

    void Start()
    {
        //cameraPitch = 0.0f;



        controller = GetComponent<CharacterController>();
        if (lockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    void Update()
    {
        if (HP > 0)
        {
            UpdateMovement();
            UpdateAnimator();
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (canAttack)
                    Attack();
            }
        }
        UpdateMouseLook();
                                 
    }

    void UpdateMouseLook()
    {
        Vector2 targetMouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        currentMouseDelta = Vector2.SmoothDamp(currentMouseDelta, targetMouseDelta, ref currentMouseDeltaVelocity, mouseSmoothTime);

        cameraPitch -= currentMouseDelta.y * mouseSensitivity;


        cameraPitch = Mathf.Clamp(cameraPitch, -90.0f, 90.0f);

        playerCamera.localEulerAngles = Vector3.right * cameraPitch;
        transform.Rotate(Vector3.up * currentMouseDelta.x * mouseSensitivity);
    }


    void UpdateMovement()
    {
        Vector2 targetDir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        // targetDir = transform.TransformDirection(targetDir);
        targetDir.Normalize();


        currentDir = Vector2.SmoothDamp(currentDir, targetDir, ref currentDirVelocity, moveSmoothTime);
        //currentDir = transform.TransformDirection(currentDir);



        if (controller.isGrounded)
        {
            animator.SetBool("PlayerJumped", false);
            velocityY = 0.0f;
            if (!Input.GetKey(KeyCode.Space))
                canJump = true;
            if (Input.GetKey(KeyCode.Space) && canJump)
                Jump();

        }
        velocityY += gravity * Time.deltaTime;

        if (Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)))
            finalSpeed = Run();
        else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
            finalSpeed = Walk();
        else
        {
            Idle();
        }

        Vector3 velocity = (transform.forward * currentDir.y + transform.right * currentDir.x) * finalSpeed + Vector3.up * velocityY;
        controller.Move(velocity * Time.deltaTime);




    }

    private void Jump()
    {
        velocityY = jumpHeight;
        animator.SetBool("PlayerJumped", true);
        AudioSource audioS = GetComponent<AudioSource>();
        audioS.PlayOneShot(jumpSound);
        canJump = false;
    }
    private float Walk()
    {
        animator.SetFloat("HorizontalVelocity", finalSpeed);
        finalSpeed = walkSpeed;
        return finalSpeed;

    }

    private float Run()
    {
        finalSpeed = walkSpeed + runSpeedIncrease;
        animator.SetFloat("HorizontalVelocity", finalSpeed);
        return finalSpeed;

    }

    private void Attack()
    {
        isAttacking = true;
        animator.SetTrigger("Attack");
        AudioSource audioS = GetComponent<AudioSource>();
        audioS.PlayOneShot(swordAttackSound);
        canAttack = false;
        StartCoroutine(ResetAttackCoolDown());



    }
    private void Idle()
    {
        animator.SetFloat("HorizontalVelocity", 0);
    }

    private void UpdateAnimator()
    {
        animator.SetFloat("VerticalVelocity", velocityY);
    }

    IEnumerator ResetAttackCoolDown()
    {
        yield return new WaitForSeconds(attackCoolDown);
        isAttacking = false;
        canAttack = true;        
        
        
    }

}