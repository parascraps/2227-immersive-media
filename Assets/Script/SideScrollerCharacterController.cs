using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.ImmersiveMedia.CharacterControl
{
    /// <summary>
    /// Controls a side scroller character
    /// </summary>
    public class SideScrollerCharacterController : IMContinousInputCharacterController
    {
        // The animator that the sidescroller character will use
        [SerializeField] private Animator animator;

        // Stores the last value retrieved from the x input axis
        private float xInputAxis = 0;
        private float yInputAxis = 0;

        protected override void Update()
        {
            base.Update();   
            UpdateAnimator();
        }

        /// <summary>
        /// Gets horizontal axis input for 
        /// character movement and animation
        /// </summary>
        protected override void GetUserInput()
        {
            base.GetUserInput();
            xInputAxis = Input.GetAxis("Horizontal");
            yInputAxis = Input.GetAxis("Vertical");
        }
        
        /// <summary>
        /// Updates variables on the animator
        /// </summary>
        private void UpdateAnimator()
        {
            animator.SetFloat("HorizontalVelocity", rb.velocity.x);
            animator.SetFloat("ForwardVelocity", rb.velocity.z);
            animator.SetFloat("VerticalVelocity", rb.velocity.y);


            animator.SetBool("Grounded", grounded);
            animator.SetBool("PlayerJumped", jump);
        }

        /// <summary>
        /// Moves the character along the x axis using a rigidbody
        /// </summary>
        protected override void MoveCharacter()
        {
            float xMoveSpeed = xInputAxis/20 * this.maxSpeed * Time.fixedDeltaTime;
            float zMoveSpeed = yInputAxis/20 * this.maxSpeed * Time.fixedDeltaTime;

            rb.velocity = new Vector3(xMoveSpeed, rb.velocity.y, zMoveSpeed);
            animator.SetFloat("HorizontalVelocity", rb.velocity.x);
            animator.SetFloat("ForwardVelocity", rb.velocity.z);
            animator.SetFloat("VerticalVelocity", rb.velocity.y); 

        }
    }
}
