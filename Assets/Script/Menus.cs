using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Menus : MonoBehaviour
{
    public void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None ;
    }
    // Start is called before the first frame update
    public void ClickRetry()
    {
        SceneManager.LoadScene("CutScene");
      
    }
    public void Endgame()
    {
        SceneManager.LoadScene("Endgame");
        
    }
    public void ClickExit()
    {
       
        Application.Quit();
    }

}
