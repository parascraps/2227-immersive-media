using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectilemove : MonoBehaviour
{
    public PlayerController hero;
    private Vector3 startP;
    private Vector3 endP;
    private LaunchMagic lm;
    
    private float desiredDuration = 0.5f;
    private float elapsedTime = 0f;
  

    // Start is called before the first frame update
    void Start()
    {
       
            endP = hero.transform.position;
            startP = transform.position;
        
    }

    // Update is called once per frame
    void Update()
    {
            
            elapsedTime += Time.deltaTime;
            float ratio = elapsedTime / desiredDuration;
            transform.position =  Vector3.Lerp(startP, endP, ratio );
        
        
    }
}
