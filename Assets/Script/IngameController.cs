using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IngameController : MonoBehaviour

    
{
    public PlayerController hero;
    public bossController boss;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindWithTag("Enemy") && boss.HP == 0)
        {
            Invoke("goToVictoryScreen", 3.0f);
        }
        else if (GameObject.FindWithTag("Player") && hero.HP == 0)
        {
            Invoke("goToEndScreen", 3.0f);
        }
    }

    void goToEndScreen()
    {
        SceneManager.LoadScene("Endgame");
    }
    void goToVictoryScreen()
    {
        SceneManager.LoadScene("Victory");
    }

}
